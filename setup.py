from setuptools import setup

setup(
    name='lang',
    version='0.1',
    packages=['langlib'],
    scripts=['bin/lang.py'],
    install_requires=[
        'ply'
    ],
    url='',
    license='',
    author='Andrey Khanov',
    author_email='',
    description='A nameless compiler for a made up language',
    zip_safe=False
)
