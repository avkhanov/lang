def emit(node):
    return node.__emit__()


class LangASTNode(object):
    _parent = None

    def __repr__(self):
        ret = self.__class__.__name__ + ":\n"

        # for x in [a for a in dir(self) if isinstance(getattr(self, a), (LangASTNode, list))]:
        for x in [a for a in dir(self) if not a.startswith('_') and not hasattr(getattr(self, a), '__call__')]:
            obj = getattr(self, x)
            ret += "\t%s = " % x

            if isinstance(obj, list):
                if len(obj) == 0:
                    ret += '[]'
                else:
                    ret += '[\n'

                    items = []

                    for i in obj:
                        items.append(str(i))

                    ret += '\t\t' + '\n\t\t'.join((",\n".join(items)).split('\n'))

                    ret += '\n\t]'
            else:
                ret += "\t" + "\n\t".join(str(obj).split('\n'))

            ret += '\n'

        return ret

    def __setattr__(self, key, value):
        if isinstance(value, LangASTNode):
            object.__setattr__(value, '_parent', self)
        elif isinstance(value, list):
            for i in value:
                object.__setattr__(i, '_parent', self)

        super().__setattr__(key, value)

    def __emit__(self):
        return "<%s>\n" % self.__class__.__name__

    def get_parent(self):
        return self._parent

    def get_ancestor(self, ancestor_type=None):
        if ancestor_type is None:
            ancestor_type = LangASTNode

        ret = self._parent

        while (not isinstance(ret, ancestor_type)) and (ret is not None):
            # print("RET: ", type(ret))
            # print("RET.P: ", type(ret._parent))
            ret = ret._parent

        return ret

    def get_symtable(self):
        if isinstance(self, Program):
            return self._symtable
        else:
            return self.get_ancestor(Program).get_symtable()


class Program(LangASTNode):
    def __init__(self, function_list, symtable):
        self.function_list = function_list
        object.__setattr__(self, '_symtable', symtable)

    def __emit__(self):
        ret = list()
        ret.append("\t.global main")

        ret.append("")
        ret.append("\t.text")
        # ret.append("\tjmp\t__start_program")

        for i in self.function_list:
            ret.append(i.__emit__())

        # ret.append("__start_program:")
        # ret.append("\tpush\t$2")
        # ret.append("\tcall\tmain")

        ret.append("\t.data")

        for i in self.function_list:
            d = self._symtable.get_strings(i)
            for label, value in d:
                ret.append("%s:\t%s" % (label, value))

        return "\n".join(ret)


class Function(LangASTNode):
    def __init__(self, id, args, type, body):
        self.id = id
        self.args = args
        self.type = type
        self.statements = body
        self._ret_label = None

    def __emit__(self):
        self.get_symtable().register_function(self)

        ret = list()
        ret.append("%s:" % self.id)
        # ret.append("\tpush\t%rbp")
        # ret.append("\tmov\t%rsp, %rbp")
        #ret.append("\tenter\t$0, $0")

        # Main Logic here
        body_coms = []

        # Process argument list
        for i in self.args:
            body_coms.append(i.__emit__())

            # offset = self.get_symtable().get_var_offset(self, i)
            # body_coms.append("\tmov\t%s(%%rbp), %%rcx" % (offset + 16))
            # body_coms.append("\tmov\t%%rcx, -%s(%%rbp)" % offset)
        # Process body
        for i in self.statements:
            body_coms.append(i.__emit__())

        # Account for any variables declared earlier
        # self._stack_size = self.get_symtable().get_stack_reserve_size(self)

        # if self._stack_size > 0:
        #     ret.append("\tsub\t$%s, %%rsp" % self._stack_size)

        # Insert the main body after the calculation
        ret.append("\tenter\t$%s, $0" % self.get_symtable().get_stack_reserve_size(self))
        ret.extend(body_coms)

        # End of function
        ret.append("%s:" % self.get_ret_label())

        # if self._stack_size > 0:
        #     ret.append("\tadd\t$%s, %%rsp" % self._stack_size)

        #ret.append("\tpop\t%rbp")
        ret.append("\tleave")
        ret.append("\tret")

        # print(ret)

        return "\n".join(ret)

    def get_ret_label(self):
        if self._ret_label is None:
            self._ret_label = "__%s_end__" % self.id

        return self._ret_label


class Decl(LangASTNode):
    def __init__(self, id, type):
        self.id = id
        self.type = type

    def __emit__(self):
        self.get_symtable().add_variable(self.get_ancestor(Function), self.id, self.type)
        return ''


class Arg(LangASTNode):
    def __init__(self, id, type):
        self.id = id
        self.type = type

    def __emit__(self):
        self.get_symtable().add_register_arg(self.get_ancestor(Function), self.id, self.type)
        return ''


class Return(LangASTNode):
    def __init__(self, expr):
        self.expr = expr

    def __emit__(self):
        func = self.get_ancestor(Function)

        ret = []
        ret.append(self.expr.__emit__())
        ret.append('\tpop\t%rax')
        ret.append('\tjmp\t%s' % func.get_ret_label())

        return "\n".join(ret)


class ForLoop(LangASTNode):
    def __init__(self, init, condition, increment, body):
        self.init = init
        self.condition = condition
        self.increment = increment
        self.body = body

    def __emit__(self):
        ret = []

        loop_start = self.get_symtable().gen_label()
        loop_end = self.get_symtable().gen_label()

        ret.append(self.init.__emit__())
        ret.append("%s:\t%s" % (loop_start, self.condition.__emit__()))
        ret.append("\tpop\t%rcx")
        ret.append("\tcmp\t$0, %rcx")
        ret.append("\tjz\t%s" % loop_end)

        if isinstance(self.body, list):
            ret.extend(map(lambda x: x.__emit__(), self.body))
        else:
            ret.append(self.body.__emit__())

        ret.append(self.increment.__emit__())
        ret.append("\tjmp\t%s" % loop_start)
        ret.append("%s:" % loop_end)

        return '\n'.join(ret)



class If(LangASTNode):
    def __init__(self, condition, body, else_body):
        self.condition = condition
        self.body = body
        self.else_body = else_body

    def __emit__(self):
        ret = []
        ret.append(self.condition.__emit__())

        end_label = self.get_symtable().gen_label()
        else_label = self.get_symtable().gen_label()

        ret.append("\tpop\t%rcx")
        ret.append("\tcmp\t$0, %rcx")
        ret.append("\tjz\t%s" % else_label)

        if isinstance(self.body, list):
            ret.extend(map(lambda x: x.__emit__(), self.body))
        else:
            ret.append(self.body.__emit__())

        ret.append("\tjmp\t%s" % end_label)
        ret.append("%s:" % else_label)

        if isinstance(self.else_body, list):
            ret.extend(map(lambda x: x.__emit__(), self.else_body))
        else:
            ret.append(self.else_body.__emit__())

        ret.append("%s:" % end_label)

        return '\n'.join(ret)


class While(LangASTNode):
    def __init__(self, condition, body):
        self.condition = condition
        self.body = body

    def __emit__(self):
        ret = []
        loop_start = self.get_symtable().gen_label()
        loop_end = self.get_symtable().gen_label()

        ret.append("%s:\t%s" % (loop_start, self.condition.__emit__()))
        ret.append("\tpop\t%rcx")
        ret.append("\tcmp\t$0, %rcx")
        ret.append("\tjz\t%s" % loop_end)

        if isinstance(self.body, list):
            ret.extend(map(lambda x: x.__emit__(), self.body))
        else:
            ret.append(self.body.__emit__)

        ret.append("\tjmp\t%s" % loop_start)
        ret.append("%s:" % loop_end)

        return '\n'.join(ret)


class Call(LangASTNode):
    def __init__(self, id, args):
        self.id = id
        self.args = args

        if len(args) > 6:
            raise Exception("Currently, 6 arguments is max")

    def __emit__(self):
        ret = []
        registers = ['rdi', 'rsi', 'rdx', 'rcx', 'r8', 'r9']

        for i in range(len(self.args)):
            ret.append(self.args[i].__emit__())
            ret.append("\tpop\t%%%s" % registers[i])

        ret.append('\txor\t%rax, %rax')
        ret.append('\tcall\t%s' % self.id.id)

        for _ in self.args:
            #ret.append('\tpop\t%rcx')
            pass

        ret.append('\tpush\t%rax')

        return '\n'.join(ret)


class TwoValExpr(LangASTNode):
    def __init__(self, lterm, rterm):
        self.lterm = lterm
        self.rterm = rterm


class Assign(TwoValExpr):
    def __emit__(self):
        ret = []

        if isinstance(self.lterm, Decl):
            ret.append(self.lterm.__emit__())
            pass
        elif isinstance(self.lterm, Id):
            pass
        else:
            raise Exception("Decl or Id expected, got %s" % type(self.lterm))

        offset = self.get_symtable().get_var_offset(self.get_ancestor(Function), self.lterm)
        print(self.rterm)
        print(self.lterm)
        ret.append(self.rterm.__emit__())
        ret.append("\tpop\t%rcx")
        ret.append("\tmov\t%%rcx, -%s(%%rbp)" % offset)

        print(ret)

        return "\n".join(ret)



class Add(TwoValExpr):
    def __emit__(self):
        ret = []
        ret.append(self.lterm.__emit__())
        ret.append(self.rterm.__emit__())
        ret.append("\tpop\t%rcx")
        ret.append("\tpop\t%rdx")
        ret.append("\taddq\t%rdx, %rcx")
        ret.append("\tpush\t%rcx")

        return "\n".join(ret)


class Sub(TwoValExpr):
    def __emit__(self):
        ret = []
        ret.append(self.rterm.__emit__())
        ret.append(self.lterm.__emit__())
        ret.append("\tpop\t%rcx")
        ret.append("\tpop\t%rdx")
        ret.append("\tsub\t%rdx, %rcx")
        ret.append("\tpush\t%rcx")

        return "\n".join(ret)


class Mult(TwoValExpr):
    pass


class Divide(TwoValExpr):
    pass


class Modulus(TwoValExpr):
    pass


class LessThan(TwoValExpr):
    def __emit__(self):
        ret = []

        ret.append(self.rterm.__emit__())
        ret.append(self.lterm.__emit__())
        ret.append("\tpop\t%rcx")
        ret.append("\tpop\t%rdx")
        ret.append("\tcmp\t%rcx, %rdx")

        true_label = self.get_symtable().gen_label()
        end_label = self.get_symtable().gen_label()

        ret.append("\tjg\t%s" % true_label)
        ret.append("\tpush\t$0")
        ret.append("\tjmp\t%s" % end_label)
        ret.append("%s:\tpush\t$1" % true_label)
        ret.append("%s:" % end_label)

        return "\n".join(ret)


class Equal(TwoValExpr):
    def __emit__(self):
        ret = []
        ret.append(self.rterm.__emit__())
        ret.append(self.lterm.__emit__())
        ret.append("\tpop\t%rcx")
        ret.append("\tpop\t%rdx")
        ret.append("\tcmp\t%rcx, %rdx")
        
        true_label = self.get_symtable().gen_label()
        end_label = self.get_symtable().gen_label()

        ret.append("\tje\t%s" % true_label)
        ret.append("\tpush\t$0")
        ret.append("\tjmp\t%s" % end_label)
        ret.append("%s:\tpush\t$1" % true_label)
        ret.append("%s:" % end_label)

        return "\n".join(ret)



class Expr(LangASTNode):
    def __init__(self, expr):
        self.expr = expr


class Incr(LangASTNode):
    def __init__(self, id):
        self.id = id


class PrimitiveNode(LangASTNode):
    def __init__(self, value):
        self.value = value


class Int(PrimitiveNode):
    def __emit__(self):
        return "\tpush\t$%s" % self.value


class Float(PrimitiveNode):
    pass


class Char(PrimitiveNode):
    pass


class Bool(PrimitiveNode):
    pass


class String(PrimitiveNode):
    def __emit__(self):
        ret = []
        label = self.get_symtable().add_string(self.get_ancestor(Function), self.value)
        ret.append("\tpush\t$%s" % label)

        return '\n'.join(ret)


class Id(LangASTNode):
    def __init__(self, value):
        self.id = value

    def __emit__(self):
        ret = []

        try:
            offset = self.get_symtable().get_var_offset(self.get_ancestor(Function), self)
            ret.append("\tmov\t-%s(%%rbp), %%rcx" % (offset))
            ret.append("\tpush\t%rcx")
        except KeyError:
            reg = self.get_symtable().get_register_arg_reg(self.get_ancestor(Function), self)
            ret.append("\tpush\t%%%s" % reg)

        return "\n".join(ret)



