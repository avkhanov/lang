import sys
import argparse

from .parser import LangParser
from .ast import emit


PROGRAM_NAME = 'lang'


def parse_arguments(argv):
    args = argparse.ArgumentParser(prog=PROGRAM_NAME)
    args.add_argument('file', help="Source file to compile")
    args.add_argument('-o', default='a.s', help="Name of output executable")

    return args.parse_args(argv)


def main(argv=sys.argv, print_ast=True):
    args = parse_arguments(argv[1:])

    with open(args.file) as f:
        data = f.read()

    p = LangParser()
    ast = p.parse(data)

    if print_ast:
        print(ast, file=sys.stderr)

    with open(args.o, 'w') as f:
        f.write(emit(ast) + '\n')

    return ast
