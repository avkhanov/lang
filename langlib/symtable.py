import random

from .ast import PrimitiveNode, Int, Float, Char, String, Bool
from .ast import Function


class FunctionEntry(object):
    def __repr__(self):
        return "%s: %s" % (self.func.id, str(self.symbols))

    def __init__(self, func:Function):
        self.func = func
        self.symbols = {}
        self.register_args = []
        self.offsets = {}
        self.strings = {}
        self.current_offset = 0


class SymbolicTableEntry(object):
    def __init__(self, symbol_name, symbol_type):
        self.symbol_name = symbol_name
        self.symbol_type = symbol_type

    def __eq__(self, other):
        return self.symbol_name == other.symbol_name


class SymbolicTable(object):
    def __init__(self):
        self.__TYPE_DEFAULTS = { 'int': Int(value=0), 'float': Float(value=0.0),
            'char': Char(value='\0'),
            'str': String(value=""),
            'bool': Bool(value=0)
        }
        self.__TYPES = {
            'int': 8,
            'float': 8,
            'char': 1,
            'string': 8,
            'bool': 1
        }

        self.__FUNCTIONS = {

        }

        self.__call_stack = []

        self.__labels = []

    def sizeof(self, type_name):
        return self.__TYPES[type_name]

    def register_function(self, func:Function):
        if func.id in self.__FUNCTIONS:
            raise Exception('%s already declared' % func.id)

        self.__FUNCTIONS[func.id] = FunctionEntry(func)

    def add_variable(self, function:Function, symname, symtype):
        if function.id not in self.__FUNCTIONS:
            raise Exception("Function '%s' has not been declared" % function.id)

        if symname in self.__FUNCTIONS[function.id].symbols or SymbolicTableEntry(symname, None) in \
                self.__FUNCTIONS[function.id].register_args:
            raise Exception("Symbol '%s' already declared in this function")

        if symtype not in self.__TYPES:
            raise Exception("Type '%s' is not defined" % symtype)

        func_entry = self.__FUNCTIONS[function.id]
        func_entry.symbols[symname] = SymbolicTableEntry(symname, symtype)
        func_entry.offsets[symname] = func_entry.current_offset
        func_entry.current_offset += self.__TYPES[symtype]

    def add_register_arg(self, function:Function, symname, symtype):
        if function.id not in self.__FUNCTIONS:
            raise Exception("Function '%s' has not been declared" % function.id)

        if symname in self.__FUNCTIONS[function.id].symbols or SymbolicTableEntry(symname, None) in \
                self.__FUNCTIONS[function.id].register_args:
            raise Exception("Symbol '%s' already declared in this function")

        if symtype not in self.__TYPES:
            raise Exception("Type '%s' is not defined" % symtype)

        func_entry = self.__FUNCTIONS[function.id]
        func_entry.register_args.append(SymbolicTableEntry(symname, symtype))

    def get_stack_reserve_size(self, function):
        if function.id not in self.__FUNCTIONS:
            raise Exception("Function '%s' is not yet declared" % function)

        # print(self.__FUNCTIONS)
        return sum([self.__TYPES[v.symbol_type] for v in self.__FUNCTIONS[function.id].symbols.values()])

    def get_var_offset(self, function, var):
        return self.__FUNCTIONS[function.id].offsets[var.id]

    def get_register_arg_reg(self, function, arg):
        registers = ['rdi', 'rsi', 'rdx', 'rcx', 'r8', 'r9']
        return registers[self.__FUNCTIONS[function.id].register_args.index(SymbolicTableEntry(arg.id, None))]

    def gen_label(self):
        chars = list(range(ord('a'), ord('z') + 1)) + list(range(ord('A'), ord('Z') + 1))
        found = False
        repeat = 0

        while not found and repeat < 10:
            ret = ""

            for i in range(10):
                ret += chr(chars[random.randint(0, len(chars) - 1)])

            if ret not in self.__labels:
                found = True

        if not found:
            raise Exception("Could not generate a unique label")

        self.__labels.append(ret)
        return ret

    def add_string(self, function, value):
        label = self.gen_label()
        self.__FUNCTIONS[function.id].strings[label] = '.ascii\t"%s\\0"' % value
        return label

    def get_strings(self, function):
        return self.__FUNCTIONS[function.id].strings.items()


    def debug_dump(self):
        import pprint
        pprint.pprint(self.__FUNCTIONS)
        pprint.pprint(self.__call_stack)
        pprint.pprint(self.__labels)
        pprint.pprint(self.__TYPES)
        pprint.pprint(self.__TYPE_DEFAULTS)

