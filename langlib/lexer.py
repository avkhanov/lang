import ply.lex as lex


class LangLexer(object):
    reserved = {
        'return': 'RETURN',
        'for': 'FOR',
        'if': 'IF',
        'else': 'ELSE',
        'break': 'BREAK',
        'while': 'WHILE'
    }

    tokens = [
        # Primitives
        'INT',
        'FLOAT',
        'CHAR',
        'BOOL',
        'STRING',

        # Identifiers
        'ID',

        # Operators
        'INCR',
        'EQ'
    ] + list(reserved.values())

    literals = [
        '+', '-', '*', '/', '%',
        '<', '>', '!',
        ',', ':', ';', '=', '(', ')', '[', ']', '{', '}'
    ]

    def t_ID(self, t):
        r'[a-zA-Z_][a-zA-Z_0-9]*'
        t.type = self.reserved.get(t.value, 'ID')
        return t

    def __init__(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    def t_INCR(self, t):
        r'\+\+'
        return t

    def t_EQ(self, t):
        r'=='
        return t

    def t_FLOAT(self, t):
        r'\d+\.\d+'
        t.value = float(t.value)
        return t

    def t_INT(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    def t_CHAR(self, t):
        r"'.'"
        t.value = t.value[1:-1]
        return t

    def t_STRING(self, t):
        r'"((\\")|[^"])*"'
        t.value = t.value[1:-1]
        return t

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_error(self, t):
        print("Illegal character '%s'" % t.value[0])

    t_ignore = ' \t'

    def test(self, data):
        self.lexer.input(data);
        while True:
            tok = self.lexer.token()
            if not tok:
                break
            print(tok)


