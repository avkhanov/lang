from .lexer import LangLexer

from .ast import Program, Function, Arg, Decl, Return, ForLoop, If, Call, Assign, \
    Add, Sub, Mult, Divide, Modulus, LessThan, Equal, Incr, Expr, \
    Int, Float, Char, Bool, String, Id, While

from ply.yacc import yacc
from .symtable import SymbolicTable


class LangParser(object):
    tokens = LangLexer.tokens

    precedence = (
        ('left', '+', '-'),
        ('left', '*', '/', '%')
    )

    def __init__(self, **kwdecls):
        self.yacc = yacc(module=self, **kwdecls)

    def parse(self, data):
        return self.yacc.parse(input=data, lexer=LangLexer().lexer)

    def p_program(self, p):
        '''program : func program
                   | func'''
        if len(p) == 2:
            # p[0] = ('program', [p[1]])
            p[0] = Program(function_list=[p[1]], symtable=SymbolicTable())
        elif len(p) == 3:
            # p[0] = ('program', [p[1]] + p[2][1])
            p[0] = Program(function_list=[p[1]] + p[2].function_list, symtable=p[2].get_symtable())

    def p_func(self, p):
        '''func : ID '(' arg_list ')' ':' ID '{' statement_list '}'
                | ID '(' ')' ':' ID '{' statement_list '}' '''
        if len(p) == 10:
            # p[0] = ('func', p[1], p[3], p[6], p[8])
            p[0] = Function(id=p[1], args=p[3], type=p[6], body=p[8])
        elif len(p) == 9:
            # p[0] = ('func', p[1], [], p[5], p[7])
            p[0] = Function(id=p[1], args=[], type=p[5], body=p[7])

    def p_func_void(self, p):
        '''func : ID '(' arg_list ')' '{' statement_list '}'
                | ID '(' ')' '{' statement_list '}' '''
        if len(p) == 8:
            # p[0] = ('func', p[1], p[3], None, p[6])
            p[0] = Function(id=p[1], args=p[3], type=None, body=p[6])
        elif len(p) == 7:
            # p[0] = ('func', p[1], [], None, p[5])
            p[0] = Function(id=p[1], args=[], type=None, body=p[5])

    def p_arg_list(self, p):
        '''arg_list : arg ',' arg_list
                     | arg '''
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = [p[1]] + p[3]

    def p_arg(self, p):
        '''arg : ID ':' ID'''

        p[0] = Arg(id=p[1], type=p[3])

    def p_decl(self, p):
        '''decl : ID ':' ID'''

        # p[0] = ('decl', p[1], p[3])
        p[0] = Decl(id=p[1], type=p[3])

    def p_statement_list(self, p):
        '''statement_list : statement statement_list
                          | '''
        if len(p) == 3:
            p[0] = [p[1]] + p[2]
        elif len(p) == 1:
            p[0] = []

    def p_statement(self, p):
        '''statement : expr ';' '''
        p[0] = p[1];

    def p_statement_return(self, p):
        '''statement : RETURN ';'
                     | RETURN expr ';' '''
        if len(p) == 3:
            val = None
        elif len(p) == 4:
            val = p[2]

        # p[0] = ('return', val)
        p[0] = Return(expr=val)

    def p_optional_statement(self, p):
        '''optional_statement : statement
                              | ';' '''
        p[0] = p[1]

    def p_statement_for_loop(self, p):
        '''statement : FOR '(' optional_expr ',' optional_expr ',' optional_expr ')' '{' statement_list '}'
                     | FOR '(' optional_expr ',' optional_expr ',' optional_expr ')' optional_statement  '''
        if len(p) == 12:
            # p[0] = ('for', p[3], p[5], p[7], p[10])
            p[0] = ForLoop(init=p[3], condition=p[5], increment=p[7], body=p[10])
        elif len(p) == 11:
            # p[0] = ('for', p[3], p[5], p[7], [p[9]])
            p[0] = ForLoop(init=p[3], condition=p[5], increment=p[7], body=[p[9]])

    def p_statement_if(self, p):
        '''statement : IF '(' expr ')' '{' statement_list '}' else
                     | IF '(' expr ')' optional_statement else
                     '''
        if len(p) == 9:
            # p[0] = ('if', p[3], p[6], p[8])
            p[0] = If(condition=p[3], body=p[6], else_body=p[8])
        elif len(p) == 7:
            # p[0] = ('if', p[3], p[5], p[7])
            p[0] = If(condition=p[3], body=p[5], else_body=p[6])

    def p_statement_while(self, p):
        '''statement : WHILE '(' expr ')' '{' statement_list '}'
                     | WHILE '(' expr ')' optional_statement'''
        if len(p) == 8:
            p[0] = While(condition=p[3], body=p[6])
        elif len(p) == 6:
            p[0] = While(condition=p[3], body=p[5])

    def p_else(self, p):
        '''else : ELSE statement
                | ELSE '{' statement_list '}'
                | '''
        if len(p) == 3:
            p[0] = [p[2]]
        elif len(p) == 5:
            p[0] = p[3]
        elif len(p) == 1:
            p[0] = []

    def p_optional_expr(self, p):
        '''optional_expr : expr
                         | '''
        p[0] = p[1]

    def p_expr(self, p):
        '''expr : id '=' expr
                | decl '=' expr
                | id INCR
                | expr '+' expr
                | expr '-' expr
                | expr '*' expr
                | expr '/' expr
                | expr '%' expr

                | expr '<' expr
                | expr EQ expr

                | '(' expr ')'
                | term
                | id '(' expr_list ')'
                | id '(' ')'
                '''
        print(p)
        print(len(p))
        if len(p) == 5:
            # p[0] = ('call', p[1], p[3])
            p[0] = Call(id=p[1], args=p[3])
        elif len(p) == 4:
            if p[2] == '(' and p[3] == ')':
                # p[0] = ('call', p[1], [])
                r = Call(id=p[1], args=[])
            elif p[2] == '=':
                # r = ('assign', p[1], p[3])
                r = Assign(lterm=p[1], rterm=p[3])
            elif p[2] == '+':
                # r = ('add', p[1], p[3])
                r = Add(lterm=p[1], rterm=p[3])
            elif p[2] == '-':
                # r = ('sub', p[1], p[3])
                r = Sub(lterm=p[1], rterm=p[3])
            elif p[2] == '*':
                # r = ('mult', p[1], p[3])
                r = Mult(lterm=p[1], rterm=p[3])
            elif p[2] == '/':
                # r = ('div', p[1], p[3])
                r = Divide(lterm=p[1], rterm=p[3])
            elif p[2] == '%':
                # r = ('mod', p[1], p[3])
                r = Modulus(lterm=p[1], rterm=p[3])

            elif p[2] == '<':
                # r = ('lt', p[1], p[3])
                r = LessThan(lterm=p[1], rterm=p[3])
            elif p[2] == '==':
                # r = ('eq', p[1], p[3])
                r = Equal(lterm=p[1], rterm=p[3])
            elif p[1] == '(':
                # r = ('expr', p[2])
                r = Expr(expr=p[2])
            else:
                raise Exception("Unknown: %s %s" % (p[1], p[2]))
            p[0] = r
        elif len(p) == 3:
            if p[2] == '++':
                # p[0] = ('incr', p[1])
                p[0] = Incr(id=p[1])
        elif len(p) == 2:
            p[0] = p[1]

    def p_expr_list(self, p):
        '''expr_list : expr ',' expr_list
                     | expr '''
        if len(p) == 4:
            p[0] = [p[1]] + p[3]
        elif len(p) == 2:
            p[0] = [p[1]]

    def p_term(self, p):
        '''term : int
                | float
                | char
                | bool
                | string
                | decl
                | id'''
        p[0] = p[1]

    def p_int(self, p):
        '''int : INT'''
        # p[0] = ('int', p[1])
        p[0] = Int(value=p[1])

    def p_float(self, p):
        '''float : FLOAT'''
        # p[0] = ('float', p[1])
        p[0] = Float(value=p[1])

    def p_char(self, p):
        '''char : CHAR'''
        # p[0] = ('char', p[1])
        p[0] = Char(value=p[1])

    def p_bool(self, p):
        '''bool : BOOL'''
        # p[0] = ('bool', p[1])
        p[0] = Bool(value=p[1])

    def p_string(self, p):
        '''string : STRING'''
        # p[0] = ('string', p[1])
        p[0] = String(value=p[1])

    def p_id(self, p):
        '''id : ID'''
        # p[0] = ('id', p[1])
        p[0] = Id(value=p[1])

    def p_error(self, p):
        print("Line %s: Unexpected token '%s'" % (p.lineno, p.value))




