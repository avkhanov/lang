import unittest
from test.langlibbase import compile_and_run


class TestWhile(unittest.TestCase):
    def test_while(self):
        r = compile_and_run("""
 main():int {
    var:int = 0;

    while(var < 5) {
        var = var + 1;
        printf("var is now %d\n", var);
    }

    return 0;
}
        """)

        self.assertEqual(r.retcode, 0)
        self.assertEqual(b"var is now 1\nvar is now 2\nvar is now 3\nvar is now 4\nvar is now 5\n", r.response)

