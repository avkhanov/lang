import unittest
from test.langlibbase import compile_and_run


class TestIf(unittest.TestCase):
    def test_if_else_if(self):
        r = compile_and_run("""
 main():int {
    var:int = 5;

    if(var == 5) {
        printf("It is a 5\n");
    } else {
        printf("It is not a 5\n");
    }

    x:int = 3;

    if(x == 2) {
        printf("It is a 2\n");
    } else if(x == 3) {
        printf("It is not a 2, but it is a 3\n");
    } else {
        printf("It is nothing! I quit\n");
    }

    return 0;
}
        """)

        self.assertEqual(r.retcode, 0)
        self.assertEqual(b"It is a 5\nIt is not a 2, but it is a 3\n", r.response)
