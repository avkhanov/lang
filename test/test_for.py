import unittest
from test.langlibbase import compile_and_run


class TestFor(unittest.TestCase):
    def test_for(self):
        r = compile_and_run("""
 main():int {
    sum:int = 0;

    for(i:int = 0, i < 10, i = i + 2) {
        sum = sum + i;
    }

    return sum;
}
        """)

        self.assertEqual(r.retcode, 20)

