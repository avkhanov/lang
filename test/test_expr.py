import unittest
from test.langlibbase import compile_and_run


class TestExpr(unittest.TestCase):
    def test_bool(self):
        r = compile_and_run("""
main():int {
    printf("%d\n", 3 == 2);
    printf("%d\n", 3 == 3);
    return 0;
}
        """)

        self.assertEqual(r.retcode, 0)
        self.assertIn(b"0\n1", r.response)

    def test_less_than(self):
        r = compile_and_run("""
main():int {
    printf("%d\n", 3 < 2);
    printf("%d\n", 3 < 4);
    printf("%d\n", 3 < 3);
    return 0;
}
        """)

        self.assertEqual(r.retcode, 0)
        self.assertEqual(b"0\n1\n0\n", r.response)
