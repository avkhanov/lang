import tempfile
import os
import stat
import subprocess
import io

from langlib.run import main


class Result(object):
    def __init__(self, response, retcode):
        self.response = response
        self.retcode = retcode


def compile_file(source_file, asm_file='a.s', output_file='a.out'):
    ast = main(['lang', source_file, '-o', asm_file], False)

    try:
        subprocess.check_call(['gcc', '-o', output_file, asm_file])
    except Exception as e:
        with open(asm_file) as f:
            print("----------------------------------")
            print("COMPILATION FAILED - ASM CONTENTS:")
            print("----------------------------------")
            print(f.read())
            print()
            print("----------------------------------")
            print("AST")
            print("----------------------------------")
            print(ast)

        raise e
    finally:
        os.unlink(asm_file)


def compile(snippet, asm_file='a.s', output_file='a.out'):
    tmpsrc = tempfile.mkstemp()[1]

    try:
        with open(tmpsrc, 'w') as f:
            print(">>>")
            print(snippet)
            print("<<<")
            f.write(snippet)

        compile_file(tmpsrc, asm_file, output_file)
    finally:
        os.unlink(tmpsrc)


def compile_and_run_helper(compile_method, source, asm_file='a.s', output_file='a.out'):
    compile_method(source, asm_file=asm_file, output_file=output_file)
    os.chmod(output_file, stat.S_IEXEC)

    retcode = 0
    output = ""

    try:
        output = subprocess.check_output(os.path.abspath(output_file))
    except subprocess.CalledProcessError as e:
        retcode = e.returncode

    return Result(output, retcode)


def compile_and_run_file(source_file, asm_file='a.s', output_file='a.out'):
    return compile_and_run_helper(compile_file, source_file, asm_file, output_file)


def compile_and_run(snippet, asm_file='a.s', output_file='a.out'):
    return compile_and_run_helper(compile, snippet, asm_file, output_file)

