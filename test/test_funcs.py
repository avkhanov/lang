import unittest
from test.langlibbase import compile_and_run

class TestFunctionCalls(unittest.TestCase):
    def test_function_calling(self):
        r = compile_and_run("""
diff(a:int, b:int) {
    return a - b;
}

main(){
    a:int = 8;
    b:int = 3;
    return diff(a, b);
}
        """)

        self.assertEqual(r.retcode, 5)

